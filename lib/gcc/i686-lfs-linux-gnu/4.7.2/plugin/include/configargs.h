/* Generated automatically. */
static const char configuration_arguments[] = "../gcc-4.7.2/configure --target=i686-lfs-linux-gnu --prefix=/tools --with-sysroot=/mnt/LFS-1 --with-newlib --without-headers --with-local-prefix=/tools --with-native-system-header-dir=/tools/include --disable-nls --disable-shared --disable-multilib --disable-decimal-float --disable-threads --disable-libmudflap --disable-libssp --disable-libgomp --disable-libquadmath --enable-languages=c --with-mpfr-include=/mnt/LFS-1/sources/gcc-4.7.2/mpfr/src --with-mpfr-lib=/mnt/LFS-1/sources/gcc-build/mpfr/src/.libs";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "pentiumpro" } };
